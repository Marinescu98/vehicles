
"""Provides some utilities widely used by other modules"""

import heapq
import math


# ______________________________________________________________________________
# Utils Functions & Class

def print_result(n,state):
    #this function print a state

    for j in range(0,len(state)):
         if j%n == n-1:
          print(state[j])
         else:
          print(state[j],end=' ')
    print('\n')

def row_col(index, vehicles):
    #the function return a tuple that contain the row and the column of vehicle which have the index 'index'

      row = math.floor(index / vehicles)
      col = index % vehicles
      return (row, col)

def create_initial_state(no_vehicles):
    #this function will crate the initial tuple

    initial_tuple=[]
    contor=1
    for i in range(0,pow(no_vehicles,2)):
        if i%no_vehicles==0:
            initial_tuple.append(contor)
            contor+=1
        else:
            initial_tuple.append(0)
    return tuple(initial_tuple)

def create_goal_state(no_vehicles):
    #this function will crate the goal tuple

    goal_tuple=[]
    contor=1
    for i in range(0,pow(no_vehicles,2)):
        if i%no_vehicles==no_vehicles-1:
            goal_tuple.append(no_vehicles-contor+1)
            contor+=1
        else:
            goal_tuple.append(0)
    return tuple(goal_tuple)

def get_move(action, number_vehicles):
    #this function will return the action

    delta = {'UP':-number_vehicles, 'DOWN':number_vehicles, 'LEFT':-1, 'RIGHT':1, 'STAY':0,'jUP':-number_vehicles*2, 'jDOWN':number_vehicles*2, 'jLEFT':-1*2, 'jRIGHT':1*2, '-':0}
    return delta[action]

class State(object):
    """ The class is responsible for changing a state. It keeps the actions of each vehicle, the current state it is always updating and the number of vehicles."""

    def __init__(self, initial, vehicles, goal):
        self.state=list(initial)
        self.number_vehicles=vehicles
        self.actions_made=[] #a list which contain the previouse action of each vehicles 
        self.goal=goal
        for j in range(1,vehicles+1):
            self.actions_made.append('-')
    
    def find_mach(self):
        #the function take 

        for j in range(1,self.number_vehicles+1):
            if self.state.index(j)==self.goal.index(j):
                self.actions_made[j-1]='Finish'

    def change_state(self, action, vehicle):
        """the function return a tuple of the state after modification according action"""

        current_position=self.state.index(vehicle)
        next_step = current_position + get_move(action,self.number_vehicles)
        self.state[current_position], self.state[next_step] = self.state[next_step], self.state[current_position]
        self.actions_made[vehicle-1]=action
        
        return tuple(self.state) 



# ______________________________________________________________________________
# Misc Functions


def memoize(fn, slot=None, maxsize=32):
    """Memoize fn: make it remember the computed value for any argument list.
    If slot is specified, store result in that slot of first argument.
    If slot is false, use lru_cache for caching the values."""
    if slot:
        def memoized_fn(obj, *args):
            if hasattr(obj, slot):
                return getattr(obj, slot)
            else:
                val = fn(obj, *args)
                setattr(obj, slot, val)
                return val
    else:
        @functools.lru_cache(maxsize=maxsize)
        def memoized_fn(*args):
            return fn(*args)

    return memoized_fn

# ______________________________________________________________________________
# Queues: Stack, FIFOQueue, PriorityQueue
# Stack and FIFOQueue are implemented as list and collection.deque
# PriorityQueue is implemented here


class PriorityQueue:
    """A Queue in which the minimum (or maximum) element (as determined by f and
    order) is returned first.
    If order is 'min', the item with minimum f(x) is
    returned first; if order is 'max', then it is the item with maximum f(x).
    Also supports dict-like lookup."""

    def __init__(self, order='min', f=lambda x: x):
        self.heap = []

        if order == 'min':
            self.f = f
        elif order == 'max':  # now item with max f(x)
            self.f = lambda x: -f(x)  # will be popped first
        else:
            raise ValueError("order must be either 'min' or max'.")

    def append(self, item):
        """Insert item at its correct position."""
        heapq.heappush(self.heap, (self.f(item), item))

    def extend(self, items):
        """Insert each item in items at its correct position."""
        for item in items:
            self.heap.append(item)

    def pop(self):
        """Pop and return the item (with min or max f(x) value
        depending on the order."""
        if self.heap:
            return heapq.heappop(self.heap)[1]
        else:
            raise Exception('Trying to pop from empty PriorityQueue.')

    def __len__(self):
        """Return current capacity of PriorityQueue."""
        return len(self.heap)

    def __contains__(self, item):
        """Return True if item in PriorityQueue."""
        return (self.f(item), item) in self.heap

    def __getitem__(self, key):
        for _, item in self.heap:
            if item == key:
                return item

    def __delitem__(self, key):
        """Delete the first occurrence of key."""
        self.heap.remove((self.f(key), key))
        heapq.heapify(self.heap)

