from search import *
from P4 import *
import time
import math


def main():

    n=int(input('Enter the number of vehicles:'))
    
    if n>2:
        #if n is greater than 2 then is possible to have a solution

        initial_state=create_initial_state(n)#crate the initial tuple
        goal_state=create_goal_state(n)#create the goal tuple

        problem_h = NVehicles(initial_state, goal_state,n)

        print('Initial state:')
        print_result(n,initial_state)
       
        print('Goal state:')
        print_result(n,goal_state)

        print('_________________________________\n')

        t1=time.time()
        if(astar_search(problem_h)==0):
            print('No solution!')
        t2=time.time()

        print('Time: ',t2-t1,' sec')
    else:
        print('No solution!')
  
if __name__ == "__main__":
    main()
