
from search import *
from utils import *
import math

class NVehicles(Problem):
    """  """
 
    def __init__(self, initial, goal, no_veh):
        """ Define goal state and initialize a problem """

        self.number_vehicles=no_veh
        self.goal = goal
        Problem.__init__(self, initial, goal,no_veh)

    def distance(self, goal_step, next_step):
         """Return the distance between two points using Euclidean Distance"""

         #it find the row and the column of each index given as parameter
         x_goal, y_goal = row_col(goal_step,self.number_vehicles)
         x_current, y_current = row_col(next_step,self.number_vehicles)

         #it calculate the distance and return it
         return math.sqrt(pow((x_goal-x_current),2)+pow((y_goal-y_current),2))

    def best_move(self, vehicle, actions, current_state):
        """Return the best action of vehicle"""

        #initialize the variables 
        min_distance=9999
        best_move = 'STAY'
        
        for action in actions:  #for each action
                     
            next_step = current_state.state.index(vehicle) + get_move(action, self.number_vehicles) #calculate the next step

            if next_step==self.goal.index(vehicle):
                #if the next step is the goal square return it 
                return action

            if (current_state.state[next_step] == 0 or action == 'STAY') and self.goal[next_step] == 0:
                # if the next square is free and it's not on the last column find the distance
                dist=self.distance(self.goal.index(vehicle),next_step)

                if dist<min_distance:
                    #verify if the distance is less then others 
                    min_distance=dist
                    best_move = action
        
        #return the move that have the min distance
        return best_move

    def move(self, current_state, vehicle):
        """ Return the best action that can be executed of vehicle, and will return it """
 
        possible_actions = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'STAY']
        index=current_state.state.index(vehicle)

        #remove left action if the vehicle in on the first column or it have
        #another vehicle on its left
        if index % self.number_vehicles == 0:
            possible_actions.remove('LEFT')
        else:
            if current_state.state[index-1]!=0:
                possible_actions.remove('LEFT')
        
        #remove up action if the vehicle in on the first row or it have
        #another vehicle above it
        if index < self.number_vehicles:
            possible_actions.remove('UP')
        else:
            if current_state.state[index-self.number_vehicles]!=0:
                possible_actions.remove('UP')

        #remove right action if the vehicle in on the last column or it have
        #another vehicle on its right
        if index % self.number_vehicles == self.number_vehicles-1:
            possible_actions.remove('RIGHT')
        else:
            if current_state.state[index+1]!=0:
                possible_actions.remove('RIGHT')

        #remove down action if the vehicle in on the last row or it have
        #another vehicle under it
        if index >= self.number_vehicles*(self.number_vehicles-1):
            possible_actions.remove('DOWN')
        else:
            if current_state.state[index+self.number_vehicles]!=0:
                possible_actions.remove('DOWN')
        
        if len(possible_actions)>1:
          possible_actions.remove('STAY')

        return self.best_move(vehicle, possible_actions, current_state)

    def jump(self, state, vehicle):
        """ Verify the possibility to make a jump, and if is possible, make that happen and return the direction of jump 
        else, if isn't possible return -1"""

        possible_jump = ['jUP', 'jDOWN', 'jLEFT', 'jRIGHT'] #all the possible jumps
        index_vehicle=state.state.index(vehicle)    #the possition of current vehicle in the state

        #if there is no vehicle over which to jump or if there is but the last move of it isn't STAY
        #remove the possibility to jump right
        if index_vehicle % self.number_vehicles == self.number_vehicles-2 or index_vehicle % self.number_vehicles == self.number_vehicles-1:
            #if the vehicle is on the penultimate column or on the last
            possible_jump.remove('jRIGHT')
        else:
            if state.state[index_vehicle+1]==0 or state.actions_made[state.state[index_vehicle+1]-1]!='STAY':
                possible_jump.remove('jRIGHT')

        #if there is no vehicle over which to jump or if there is but the last move of it isn't STAY
        #remove the possibility to jump LEFT
        if index_vehicle % self.number_vehicles <= 1:
            #if the vehicle is on the first or the second column
            possible_jump.remove('jLEFT')
        else:
            if state.state[index_vehicle-1]==0 or state.actions_made[state.state[index_vehicle-1]-1]!='STAY':
                possible_jump.remove('jLEFT')

        #if there is no vehicle over which to jump or if there is but the last move of it isn't STAY
        #remove the possibility to jump UP
        if index_vehicle < self.number_vehicles*2:
            #if the vehicle is on the first or the second row
            possible_jump.remove('jUP')
        else:
            if state.state[index_vehicle-self.number_vehicles]==0 or state.actions_made[state.state[index_vehicle-self.number_vehicles]-1]!='STAY':
                possible_jump.remove('jUP')     

        #if there is no vehicle over which to jump or if there is but the last move of it isn't STAY
        #remove the possibility to jump DOWN
        if index_vehicle >= self.number_vehicles*(self.number_vehicles-2):
            #if the vehicle is not the last or on the penultimate row
            possible_jump.remove('jDOWN') 
        else:
            if state.state[index_vehicle+self.number_vehicles]==0 or state.actions_made[state.state[index_vehicle+self.number_vehicles]-1]!='STAY':
                possible_jump.remove('jDOWN')  

        if len(possible_jump)==0:
            return -1
        else:
            return self.best_move(vehicle, possible_jump, state)

    def actions(self, state):
        """Return the list with all the possible states"""

        possible_states = [] 

        for i in range(1,self.number_vehicles+1):
            
            current_state=State(state,self.number_vehicles,self.goal)
            current_state.find_mach()# mark the vehicles which are on the goal square

            if current_state.actions_made[i-1] != 'Finish':
                #if vehicle it's not on its goal square
                
                actionjumpFor=self.jump_or_move(current_state,i)
                current_state.change_state(actionjumpFor,i)
                
                for j in range(1,self.number_vehicles+1):
                    #for all vehicles find all the possible actions and choose the better one

                    if j!=i and current_state.state.index(j) != self.goal.index(j):
                        action=self.jump_or_move(current_state,j)
                        current_state.change_state(action,j)#do the move

                possible_states.append(current_state)

        return possible_states

    def jump_or_move(self, current_state, vehicle):
        #verify what king of move can make: jump or a normal move

        find_a_jump= self.jump(current_state, vehicle)

        if find_a_jump==-1:
            #if it's not possible to jump, move
            action=self.move(current_state,vehicle)
            return action

        return find_a_jump

    def h(self, node): 
        #heurisitc function

        sum_of_distance=0
        for j in range(1,self.number_vehicles+1):
            sum_of_distance += math.fabs(self.goal.index(j)-node.state.index(j))
            if self.goal.index(j)!=node.state.index(j):
                sum_of_distance+=1

        return sum_of_distance
